#!/usr/bin/python

#
# Name: tree.py
#
# Description: This implements a multi-way tree data structure.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#
# Date: 9 September 2016
#

class Tree :
    def make_node(self, tag) :
        # Make variables and functions visible in the inner Class
        #
        tag_t     = self._tag_t
        make_node = self.make_node

        class Node :
            _tag    = None
            _child  = []

            def __init__(self, tag) :
                if type(tag) is tag_t :
                    self._tag = tag
                else :
                    print "ERROR: Invalid type for tag argument"

                    raise TypeError

            def add_child(self, leaf) :
                if type(leaf) is tag_t :
                    self._child.append(make_node(leaf))
                else :
                    print "ERROR: Invalid type for leaf argument"

                    raise TypeError

                return self._child[-1]

            def __str__(self) :
                retStr  = "_tag: <" + str(self._tag) + "> len(_child): " + str(len(self._child))

                return retStr

        return Node(tag)

    def __init__(self, tag_t, root_tag) :
        self._root   = None
        self._tag_t  = None

        if type(tag_t) is type :
            self._tag_t  = tag_t
        else :
            print "ERROR: Invalid type for tag_t argument"

            raise TypeError

        # make_node will verify root_tag is of the correct type
        #
        self._root   = self.make_node(root_tag)

    def __str__(self) :
        retStr  = "<" + str(self._root._tag) + ">\n"
        retStr += self.__str_step__(self._root)

        return retStr

    def __str_step__(self, root, lvl=0) :
        retStr = ""

        for i in root._child :
            str_list = str(i._tag).split("\n")

            for j in range(len(str_list)) :
                if j == 0 :
                    retStr += (" |  " * lvl) + " |- <" + str(str_list[j]) + ">\n"
                else :
                    retStr += (" |  " * lvl) + " |  ... <" + str(str_list[j]) + ">\n"

            retStr += self.__str_step__(i, lvl + 1)

        return retStr

    def search_leaf(self, leaf_tag) :
        return self.__search(leaf_tag, True)
    def search_child(self, leaf_tag) :
        return self.__search(leaf_tag, False)

    def __search(self, leaf_tag, edge=False) :
        route = self.__search_step__(self._root)

        for i in route :
            if edge == True :
                i_list = [-1]
            else :
                i_list = range(len(i))

            for j in i_list :
                if i[j]._tag == leaf_tag :
                    i.insert(0, self._root)

                    return i

        return None

    def search_all_leaf(self, leaf_tag) :
        return self.__search_all(leaf_tag, True)

    def search_all_child(self, leaf_tag) :
        return self.__search_all(leaf_tag, False)

    def __search_all(self, leaf_tag, edge=False) :
        ret = []

        route = self.__search_step__(self._root)

        for i in route :
            if edge == True :
                i_list = [-1]
            else :
                i_list = range(len(i))

            for j in i_list :
                if i[j]._tag == leaf_tag :
                    i.insert(0, self._root)

                    ret.append(i)

                    break

        if len(ret) != 0 :
            return ret
        else :
            return None

    def __search_step__(self, root) :
        route = []
        last  = True

        for i in root._child :
            last = False

            for childRoute in self.__search_step__(i) :
                childRoute.insert(0, i)

                route.append(childRoute)

        if last is True :
            route.append([])

        return route
