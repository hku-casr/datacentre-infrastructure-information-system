#!/usr/bin/python

#
# Name: graph.py
#
# Description: This implements a multi-graph data structure.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#
# Date: 9 September 2016
#

class Graph :
    def _make_node(self, tag, max_edge) :
        # Make variables and functions visible in the inner Class
        #
        tag_t          = self._tag_t
        slot_t         = self._slot_t
        make_connector = self._make_connector
        slot_neg       = self._slot_neg
        slot_split     = self._slot_split

        class Node :
            _tag      = None
            _max_edge = None
            _min_edge = None
            _slot_split = None
            _slot     = []

            def __init__(self, tag, max_edge) :
                if type(tag) is tag_t :
                    self._tag = tag
                else :
                    print "ERROR: Invalid type for tag argument"

                    raise TypeError

                if slot_t is str :
                    self._slot = dict()
                else :
                    self._slot = []

                    if type(max_edge) is int or type(max_edge) is str :
                        max_edge = int(max_edge)

                        self._max_edge = max_edge
                    else :
                        print "ERROR: Invalid type for max_edge argument"

                        raise TypeError

                    self._slot = [None] * (max_edge + 1)

                    if slot_neg == True :
                        self._min_edge = -5

                        self._slot[0] = [None]
                        # self._slot[0] = [None] * (-self._min_edge)
                    else :
                        self._min_edge = 0

                    if slot_split == True :
                        self._slot_split = True
                    else :
                        self._slot_split = False

            def add_edge(self, src_slot, dest_node, dest_slot) :
                if slot_t is int :
                    if type(src_slot) is int or type(src_slot) is str :
                        src_slot = int(src_slot)

                        if slot_neg == False and src_slot < 0 :
                            print "ERROR: Invalid value for src_slot argument"

                            raise ValueError
                    else :
                        print "ERROR: Invalid type for src_slot argument"

                        raise TypeError

                    if src_slot > 0 :
                        src_handle = self._slot
                    elif slot_neg == True :
                        src_handle = self._slot[0]
                    else :
                        src_handle = self._slot
                elif type(src_slot) is slot_t :
                    if src_slot in self._slot :
                        src_handle = self._slot
                    else :
                        self._slot[src_slot] = None

                        src_handle = self._slot
                else :
                    print "ERROR: Invalid type for src_slot argument"

                    raise TypeError

                if type(dest_node._slot) is list :
                    if type(dest_slot) is int or type(dest_slot) is str :
                        dest_slot = int(dest_slot)

                        if dest_node._min_edge == 0 and dest_slot < 0 :
                            print "ERROR: Invalid value for dest_slot argument"

                            raise ValueError
                    else :
                        print "ERROR: Invalid type for dest_slot argument"

                        raise TypeError

                    if dest_slot > 0 :
                        dest_handle = dest_node._slot
                    elif dest_node._min_edge < 0 :
                        dest_handle = dest_node._slot[0]
                    else :
                        dest_handle = dest_node._slot
                elif type(dest_node._slot) is dict :
                    if dest_slot in dest_node._slot :
                        dest_handle = dest_node._slot
                    else :
                        dest_node._slot[dest_slot] = None

                        dest_handle = dest_node._slot
                else :
                    print "ERROR: Invalid type for slot container of the node"

                    raise TypeError

                src_conn = make_connector(dest_node, dest_slot)
                dest_conn = make_connector(self, src_slot)

                if type(src_slot) is int :
                    src_slot = abs(src_slot)

                if type(dest_slot) is int :
                    dest_slot = abs(dest_slot)

                for handle, slot, conn, split in [ (src_handle, src_slot, src_conn, self._slot_split), (dest_handle, dest_slot, dest_conn, dest_node._slot_split) ] :
                    if type(handle) is list :
                        while len(handle) < slot + 1 :
                            handle.append(None)

                    if handle[slot] is None :
                        handle[slot] = conn
                    elif split == True and type(handle[slot]) is list :
                        handle[slot].append(conn)
                    elif split == True :
                        handle[slot] = [handle[slot]]
                        handle[slot].append(conn)
                    else :
                        print "ERROR: Repeated port assignment"

                        raise ValueError

            def __str__(self) :
                retStr  = "_tag: <" + str(self._tag) + "> len(_slot): " + str(len(self._slot)) + "\n            "

                lower_none = None
                upper_none = None

                if type(self._slot) is list :
                    if self._min_edge < 0 :
                        next_start = 1

                        for i in list(reversed(range(len(self._slot[0])))) :
                            if self._slot[0][i] is None :
                                if lower_none is None :
                                    lower_none = upper_none = -i
                                else :
                                    upper_none = -i
                            else :
                                # IDENTICAL BLOCK
                                #
                                if lower_none is not None :
                                    retStr += str(lower_none) + ": " + "\n            "

                                    if abs(upper_none - lower_none) == 1 :
                                        retStr += str(upper_none) + ": " + "\n            "
                                    elif abs(upper_none - lower_none) > 1 :
                                        retStr += "|   ...\n            "
                                        retStr += str(upper_none) + ": " + "\n            "

                                    lower_none = upper_none = None
                                #
                                # END OF IDENTICAL BLOCK

                                if type(self._slot[0][i]) is list :
                                    for j in range(len(self._slot[0][i])) :
                                        retStr += ("" if i == 0 else "-") + str(i) + "." + chr(65 + j) + ": " + str(self._slot[0][i][j]) + "\n            "
                                else :
                                    retStr += ("" if i == 0 else "-") + str(i) + ": " + str(self._slot[0][i]) + "\n            "
                    else :
                        next_start = 0

                    for i in range(next_start, len(self._slot)) :
                        if self._slot[i] is None :
                            if lower_none is None :
                                lower_none = upper_none = i
                            else :
                                upper_none = i
                        else :
                            # IDENTICAL BLOCK
                            #
                            if lower_none is not None :
                                retStr += str(lower_none) + ": " + "\n            "

                                if abs(upper_none - lower_none) == 1 :
                                    retStr += str(upper_none) + ": " + "\n            "
                                elif abs(upper_none - lower_none) > 1 :
                                    retStr += "|   ...\n            "
                                    retStr += str(upper_none) + ": " + "\n            "

                                lower_none = upper_none = None
                            #
                            # END OF IDENTICAL BLOCK

                            if type(self._slot[i]) is list :
                                for j in range(len(self._slot[i])) :
                                    retStr += str(i) + "." + chr(65 + j) + ": " + str(self._slot[i][j]) + "\n            "
                            else :
                                retStr += str(i) + ": " + str(self._slot[i]) + "\n            "

                    # IDENTICAL BLOCK
                    #
                    if lower_none is not None :
                        retStr += str(lower_none) + ": " + "\n            "

                        if abs(upper_none - lower_none) == 1 :
                            retStr += str(upper_none) + ": " + "\n            "
                        elif abs(upper_none - lower_none) > 1 :
                            retStr += "|   ...\n            "
                            retStr += str(upper_none) + ": " + "\n            "

                        lower_none = upper_none = None
                    #
                    # END OF IDENTICAL BLOCK
                elif type(self._slot) is dict :
                    for i in self._slot :
                        retStr += str(i) + ": " + str(self._slot[i]) + "\n            "

                return retStr

            def __eq__(self, other) :
                if self._tag == other._tag :
                    return True
                else :
                    return False

        retNode = Node(tag, max_edge)

        if self._Node_t is None :
            self._Node_t = type(retNode)

        return retNode

    def _make_connector(self, node, slot) :
        Node_t = self._Node_t
        slot_t = self._slot_t

        class Connector :
            _node   = None
            _slot   = None

            def __init__(self, node, slot) :
                if type(node) is Node_t :
                    self._node = node
                else :
                    print "ERROR: Invalid type for node argument"

                    raise TypeError

                if type(node._slot) is list :
                    if type(slot) is int or type(slot) is str :
                        slot = int(slot)

                        if slot > node._max_edge :
                            print "ERROR: slot out of upper bound"

                            raise ValueError

                        if slot < node._min_edge :
                            print "ERROR: slot out of lower bound"

                            raise ValueError

                        self._slot = slot
                    else :
                        print "ERROR: Invalid type for slot argument"

                        raise TypeError
                elif type(node._slot) is dict :
                    self._slot = slot
                else :
                    print "ERROR: Invalid type for slot container of the node"

                    raise TypeError

            def __str__(self) :
                retStr  = "_node: <" + str(self._node._tag) + " _slot: " + str(self._slot) + ">"

                return retStr

            def __eq__(self, other) :
                if self._node == other._node and self._slot == other._slot :
                    return True
                else :
                    return False

        return Connector(node, slot)

    def __init__(self, tag_t, slot_t=int, slot_neg=False, slot_split=False) :
        self._tag_t  = None
        self._Node_t = None
        self._slot_t = None
        self._slot_neg = None
        self._slot_split = None
        self._node   = []

        if type(tag_t) is type :
            self._tag_t  = tag_t
        else :
            print "ERROR: Invalid type for tag_t argument"

            raise TypeError

        if type(slot_t) is type :
            self._slot_t  = slot_t
        else :
            print "ERROR: Invalid type for slot_t argument"

            raise TypeError

        if type(slot_neg) is bool :
            self._slot_neg = slot_neg
        else :
            print "ERROR: Invalid type for slot_neg argument"

            raise TypeError

        if type(slot_split) is bool :
            self._slot_split = slot_split
        else :
            print "ERROR: Invalid type for slot_split argument"

            raise TypeError

    def add_node(self, tag, max_edge=0) :
        for i in self._node :
            if tag == i._tag :
                print "WARNING: Node already exist. Might indicate invalid definitions from source"

                return None

        self._node.append(self._make_node(tag, max_edge))

        return self._node[-1]

    def search_node(self, tag) :
        for i in self._node :
            if i._tag == tag :
                return i

        return None

    def __str__(self) :
        retStr  = "_node: "

        for i in self._node :
            retStr += "<" + str(i) + ">\n       "

        return retStr
