#!/usr/bin/python

#
# Name: datacentre.py
#
# Description: This implements a Datacentre library in Python that parses the
#  XML instances of datacentres, defined according to the XML Schema in the
#  Datacentre Infrastructure Information System. The library parses for device
#  entities and components hierarchy and parses network and power wirings.
#
# Author: C.-H. Dominic HUNG <chdhung@hku.hk>
#  Computer Architecture and System Research,
#  Department of Electrical and Electronic Engineering,
#  The University of Hong Kong
#
# Date: 4 September 2016
#

import xml.etree.ElementTree as ET
from tree.tree import Tree
from graph.graph import Graph

class Datacentre :
    class AccountAux :
        def __init__(self) :
            self._acct = None

        def _add_account(self, username, password, authtype) :
            acct = Datacentre.Account(username, password, authtype)

            if self._acct == None :
                self._acct = acct
            elif type(self._acct) is list :
                self._acct.append(acct)
            elif type(self._acct) is Datacentre._acct_t :
                self._acct = [self._acct]

                self._acct.append(acct)

        def _list_account(self) :
            if self._acct == None :
                return None
            elif type(self._acct) is list :
                retStr = []

                for i in self._acct :
                    retStr.append(str(i))

            elif type(self._acct) is Datacentre._acct_t :
                retStr = [str(self._acct)]

            return retStr

    class DC_Element(AccountAux) :
        def __init__(self, dev_name, dev_type, existence=None, opsys=None) :
            self._name  = None
            self._type  = None
            self._exist = None
            self._opsys = None
            self._acct  = None
            self._ndev   = None
            self._ninf   = None

            if type(dev_name) is str :
                self._name = dev_name
            else :
                print "ERROR: Invalid type for dev_name argument"

                raise TypeError

            if type(dev_type) is str :
                self._type = dev_type
            else :
                print "ERROR: Invalid type for dev_type argument"

                raise TypeError

            if existence is None :
                self._exist = existence
            elif type(existence) is str :
                if existence == "physical" or existence == "logical" :
                    self._exist = existence
                else :
                    print "ERROR: existence value failed predicate test"

                    raise ValueError
            else :
                print "ERROR: Invalid type for existence argument"

                raise TypeError

            if opsys is None :
                self._opsys = opsys
            elif type(opsys) is str :
                self._opsys = opsys
            else :
                print "ERROR: Invalid type for opsys argument"

                raise TypeError

        def _add_layer2_device(self, dev_id, dev_type, dev_addr) :
            ndev = Datacentre.Layer2Device(dev_id, dev_type, dev_addr)

            if self._ndev == None :
                self._ndev = [ndev]
            elif type(self._ndev) is list :
                self._ndev.append(ndev)

        def _add_layer3_interface(self, dev_id, dev_type, if_type, if_addr) :
            ninf = Datacentre.Layer3Interface(dev_id, dev_type, if_type, if_addr)

            if self._ninf == None :
                self._ninf = [ninf]
            elif type(self._ninf) is list :
                self._ninf.append(ninf)

        def __str__(self) :
            retStr  = "_name: " + self._name + " _type: " + self._type

            acct = self._list_account()

            if acct is not None :
                for i in acct :
                    retStr += "\n" + str(i)

            return retStr

        def __eq__(self, other) :
            if self._name == other._name :
                return True
            else :
                return False

    class Account :
        def __init__(self, username, password, authtype) :
            self._username = None
            self._password = None
            self._authtype = None

            if type(username) is str :
                self._username = username
            else :
                print "ERROR: Invalid type for username argument"

                raise TypeError

            if type(password) is str :
                self._password = password
            else :
                print "ERROR: Invalid type for password argument"

                raise TypeError

            if type(authtype) is str :
                if authtype == "admin" or authtype == "power_user" or authtype == "user" :
                    self._authtype = authtype
                else :
                    print "ERROR: authtype value failed predicate test"

                    raise ValueError
            else :
                print "ERROR: Invalid type for authtype argument"

                raise TypeError

        def __str__(self) :
            retStr = "_auth: " + self._authtype + " _username: " + self._username + " _password: " + self._password

            return retStr

    class Service(AccountAux) :
        def __init__(self, name) :
            self._name = None
            self._acct = None

            if type(name) is str :
                self._name = name
            else :
                print "ERROR: Invalid type for name argument"

                raise TypeError

        def __str__(self) :
            retStr  = ""
            baseStr = "_name: " + self._name

            acct = self._list_account()

            if acct is not None :
                for i in acct :
                    retStr += baseStr + " " + i
            else :
                retStr = baseStr

            return retStr

    class Layer2Device :
        def __init__(self, dev_id, dev_type, dev_addr) :
            self._addr = None
            self._type = None
            self._id   = None

            if type(dev_addr) is str :
                self._addr = dev_addr
            else :
                print "ERROR: Invalid type for dev_addr argument"

                raise TypeError

            if type(dev_type) is str :
                if dev_type == "ethernet" or dev_type == "interconnect" or dev_type == "fibreChannel" :
                    self._type = dev_type
                else :
                    print "ERROR: dev_type value failed predicate test"

                    raise ValueError
            else :
                print "ERROR: Invalid type for dev_type argument"

                raise TypeError

            if type(dev_id) is int or type(dev_id) is str :
                self._id = int(dev_id)
            else :
                print "ERROR: Invalid type for dev_id argument"

                raise TypeError

        def __str__(self) :
            retStr = " _addr: " + str(self._addr) + " _type: " + self._type

            return retStr

    class Layer3Interface :
        def __init__(self, dev_id, dev_type, if_type, if_addr) :
            self._addr = None
            self._type = None
            self._ver  = None
            self._id   = None

            if type(if_addr) is str :
                self._addr = if_addr
            else :
                print "ERROR: Invalid type for if_addr argument"

                raise TypeError

            if type(dev_type) is str :
                if dev_type == "ethernet" or dev_type == "interconnect" or dev_type == "fibreChannel" :
                    self._type = dev_type
                else :
                    print "ERROR: dev_type value failed predicate test"

                    raise ValueError
            else :
                print "ERROR: Invalid type for dev_type argument"

                raise TypeError

            if type(if_type) is int or type(if_type) is str :
                if_type = int(if_type)

                if if_type == 4 :
                    self._ver = "ip_v4"
                elif if_type == 6 :
                    self._ver = "ip_v6"
                else :
                    print "ERROR: if_type value failed predicate test"

                    raise ValueError
            else :
                print "ERROR: Invalid type for if_type argument"

                raise TypeError

            if type(dev_id) is int or type(dev_id) is str :
                self._id = int(dev_id)
            else :
                print "ERROR: Invalid type for dev_id argument"

                raise TypeError

        def _ip_v4_is_subnet(self, subnet, netmask="255.255.255.0") :
            subnet  = Datacentre.Layer3Interface.__ip_v4_parse(subnet)
            netmask = Datacentre.Layer3Interface.__ip_v4_parse(netmask)
            addr    = Datacentre.Layer3Interface.__ip_v4_parse(self._addr)
            addr_m  = [None] * 4
            subn_m  = [None] * 4

            for i in range(4) :
                addr_m[i] = addr[i] & netmask[i]
                subn_m[i] = subnet[i] & netmask[i]

            return addr_m == subn_m

        @classmethod
        def __ip_v4_parse(self, addr) :
            if type(addr) is str :
                addr = addr.split(".")

                if len(addr) == 4 :
                    for i in range(len(addr)) :
                        addr[i] = int(addr[i])

                        if addr[i] < 0 and addr[i] > 255 :
                            print "ERROR: addr value failed predicate test"

                            raise ValueError
                else :
                    print "ERROR: addr value failed predicate test"

                    raise ValueError
            else :
                print "ERROR: Invalid type for addr argument"

                raise TypeError

            return addr

        def __str__(self) :
            retStr = " _addr: " + str(self._addr) + " _ver: " + self._ver

            return retStr

    def _initialise_tree(self, xml_root) :
        if type(self._flat_map) is not dict or type(self._flat_lst) is not dict :
            print "ERROR: Mapping of flattening referrals has not been populated"

            raise ValueError

        root_node = self.DC_Element(xml_root.get("dcID"), "datacentre", "physical")

        self._tree = Tree(type(root_node), root_node)
        self._acct = dict()
        self._srvc = dict()
        self._ndev = dict()
        self._ninf = dict()

        self.__dc_element_dfs_step(xml_root, self._tree._root)

    def __dc_element_dfs_step(self, xml_parent, parent_node) :
        for t in [ "system", "device", "switch", "pdu", "ups", "operatingSystem", "service" ] :
            for i in xml_parent.findall("./" + t) :
                if t == "pdu" or t == "ups" :
                    ident = i.get("unitID")
                    exist = "physical"
                    opsys = None
                elif t == "operatingSystem" :
                    ident = i.find("hostname").text
                    exist = None
                    opsys = i.find("distribution").text + " " + i.find("version").text
                elif t == "service" :
                    ident = i.find("name").text
                    exist = None
                    opsys = None
                else :
                    ident = i.get("deviceID")
                    exist = i.get("existence")
                    opsys = None

                child      = self.DC_Element(ident, t, exist)
                child_node = parent_node.add_child(child)

                if t == "service" :
                    uid = parent_node._tag._name + ":" + ident
                    self._srvc[uid] = child_node

                for j in i.findall("account") :
                    uid = ident + ":" + j.find("username").text

                    self._acct[uid] = child_node

                    if self._admin is True :
                        child._add_account(j.find("username").text, j.find("password").text, j.get("auth"))

                for d_t, d_addr_t in [ ("ethernet", "mac_addr"), ("interconnect", "guid"), ("fibreChannel", "wwn"), ("adapter", None) ] :
                    for d in i.findall("./" + d_t) :
                        if d_addr_t != None :
                            d_addr_p = d.find(d_addr_t)
                        else :
                            for d_addr_t in [ "mac_addr", "guid", "wwn" ] :
                                d_addr_p = d.find(d_addr_t)

                                if d_addr_p is not None :
                                    break

                        if d_addr_p is None :
                            d_addr = "UNKNOWN / UNDEFINED"
                        else :
                            d_addr = d_addr_p.text

                        d_id   = d.get("id")

                        if d_t == "adapter" :
                            d_t = d.get("type")

                        child._add_layer2_device(d_id, d_t, d_addr)

                        if t == "service" :
                            ident = parent_node._tag._name
   
                        if ident in self._flat_map :
                            ident = self._flat_map[ident]

                        uid = ident + ":" + d_t + ":" + d_id

                        ndev_id  = len(child._ndev) - 1
                        ndev_tup = (child_node, ndev_id)

                        self._ndev[uid] = ndev_tup

                for inf in i.findall("./interface") :
                    inf_id   = inf.get("portID")
                    inf_id_t = inf.get("type")

                    for inf_addr in inf.findall("ip_addr") :
                        inf_saddr = inf_addr.text
                        inf_t     = inf_addr.get("version")

                        child._add_layer3_interface(inf_id, inf_id_t, inf_t, inf_saddr)

                        if t == "service" or t == "operatingSystem" :
                            ident = parent_node._tag._name
   
                        if ident in self._flat_map :
                            ident = self._flat_map[ident]

                        uid = ident + ":" + inf_id_t + ":" + inf_id

                        ninf_id  = len(child._ninf) - 1
                        ninf_tup = (child_node, ninf_id)

                        if uid in self._ninf :
                            if type(self._ninf[uid]) is list :
                                self._ninf[uid].append(ninf_tup)
                            else :
                                self._ninf[uid] = [self._ninf[uid]]

                                self._ninf[uid].append(ninf_tup)
                        else :
                            self._ninf[uid] = ninf_tup

                self.__dc_element_dfs_step(i, child_node)

    def _initialise_flatten_map(self, xml_root) :
        self._flat_map = dict()
        self._flat_lst = dict()

        self.__flatten_map_dfs_step(xml_root)

    def __flatten_map_dfs_step(self, xml_parent, upper_route=[]) :
        if type(upper_route) is not list :
            return None

        last = True

        for t in ["system", "device", "switch", "pdu", "ups"] :
            for i in xml_parent.findall("./" + t) :
                last = False

                route = list(upper_route)

                if i.tag == "pdu" or i.tag == "ups" :
                    ident = i.get("unitID")
                else :
                    ident = i.get("deviceID")

                flatID = i.get("flattenID")

                if flatID is not None :
                    if flatID in route :
                        self._flat_map[ident] = flatID

                        if flatID not in self._flat_lst :
                            self._flat_lst[flatID] = []

                        self._flat_lst[flatID].append(ident)

                route.append(ident)

                self.__flatten_map_dfs_step(i, route)

    def _initialise_graph(self, xml_root) :
        if type(self._flat_map) is not dict or type(self._flat_lst) is not dict :
            print "ERROR: Mapping of flattening referrals has not been populated"

            raise ValueError

        root_node = self.DC_Element(xml_root.get("dcID"), "datacentre", "physical")

        self._pwr_graph = Graph(type(root_node), slot_neg=True, slot_split=True)
        self._net_graph = Graph(type(root_node), slot_neg=True, slot_split=False)

        self._dev_pwr_graph = Graph(type(root_node), slot_neg=True, slot_split=True)
        self._dev_net_graph = Graph(type(root_node), slot_neg=True, slot_split=False)

        self._ext_graph = Graph(type(root_node), str)
        self._ext_graph.add_node(self.DC_Element("backbone", "switch", "physical"), 999)
        self._ext_graph.add_node(self.DC_Element("main", "pdu", "physical"), 999)

        self.__graph_dfs_step(xml_root)

        self.__graph_add_power_wire(xml_root)
        self.__graph_add_network_wire(xml_root)

    def __graph_dfs_step(self, xml_parent) :
        for t in [ "system", "device", "switch", "pdu", "ups" ] :
            for i in xml_parent.findall("./" + t) :
                if t == "pdu" or t == "ups" :
                    ident = i.get("unitID")
                    exist = "physical"
                    graph = self._pwr_graph
                    sec   = None
                    slot  = i.get("num_outlet")
                elif t == "switch" :
                    ident = i.get("deviceID")
                    exist = i.get("existence")
                    graph = self._net_graph
                    sec   = self._dev_pwr_graph
                    slot  = i.get("num_port")
                else :
                    ident = i.get("deviceID")
                    exist = i.get("existence")
                    graph = self._dev_pwr_graph
                    sec   = self._dev_net_graph
                    slot  = 99

                if ident in self._flat_map :
                    ident = self._flat_map[ident]

                graph.add_node(self.DC_Element(ident, t, exist), slot)

                if sec is not None :
                    sec.add_node(self.DC_Element(ident, t, exist), slot)

                self.__graph_dfs_step(i)

    def __graph_add_power_wire(self, xml_root) :
        for t1, t2 in [ ("power", "deviceID"), ("outlet", "unitID") ] :
            for i in xml_root.findall(".//" + t1 + "/next_hop/../..") :
                for j in i.findall("./" + t1) :
                    for k in j.findall("./next_hop") :
                        a_ident = i.get(t2)

                        if a_ident in self._flat_map and j.get("existence") == "physical":
                            a_ident = self._flat_map[a_ident]
                        elif a_ident in self._flat_map:
                            print "WARNING: Drop logical connection at ", i.get(t2), " id=", j.get("id")

                            continue

                        b_ident = k.get("unitID")

                        if b_ident in self._flat_map and j.get("existence") == "physical":
                            b_ident = self._flat_map[b_ident]
                        elif b_ident in self._flat_map:
                            print "WARNING: Drop logical connection at ", i.get(t2), " id=", j.get("id")

                            continue

                        a = (a_ident, j.get("id"))
                        b = (b_ident, k.get("outletID"))

                        a_ident, a_id = a
                        a_node = self.DC_Element(a_ident, "device", "physical")
                        b_ident, b_id = b
                        b_node = self.DC_Element(b_ident, "device", "physical")

                        if self._pwr_graph.search_node(a_node) is not None :
                            src  = a
                            src_node = self._pwr_graph.search_node(a_node)
                            dest = b
                            dest_node = b_node

                            src_graph = self._pwr_graph
                        elif self._pwr_graph.search_node(b_node) is not None :
                            src  = b
                            src_node = self._pwr_graph.search_node(b_node)
                            dest = a
                            dest_node = a_node

                            src_graph = self._pwr_graph
                        elif self._ext_graph.search_node(a_node) is not None :
                            src  = a
                            src_node = self._ext_graph.search_node(a_node)
                            dest = b
                            dest_node = b_node

                            src_graph = self._ext_graph
                        elif self._ext_graph.search_node(b_node) is not None :
                            src  = b
                            src_node = self._ext_graph.search_node(b_node)
                            dest = a
                            dest_node = a_node

                            src_graph = self._ext_graph
                        else :
                            print "ERROR: Power net parse observed a mis-connection"

                            raise ValueError

                        src_ident, src_id   = src
                        dest_ident, dest_id = dest

                        if self._pwr_graph.search_node(dest_node) is not None :
                            dest_graph = self._pwr_graph
                        elif self._dev_pwr_graph.search_node(dest_node) is not None :
                            dest_graph = self._dev_pwr_graph
                        elif self._ext_graph.search_node(dest_node) is not None :
                            dest_graph = self._ext_graph
                        else :
                            print "ERROR: Power net parse observed a mis-connection"

                            raise ValueError

                        dest_node = dest_graph.search_node(dest_node)

                        src_node.add_edge(src_id, dest_node, dest_id)

    def __graph_add_network_wire(self, xml_root) :
        l1_p1 = "/next_hop/../.."
        l1_p2 = "/next_hop/../../../.."

        l2_p1 = "./"
        l2_p2 = "./service/simplePort/"

        for t1, l1_p, l2_p in [ ("ethernet", l1_p1, l2_p1), ("interconnect", l1_p1, l2_p1), ("fibreChannel", l1_p1, l2_p1), ("port", l1_p1, l2_p1), ("ethernetPort", l1_p1, l2_p1), ("interconnectPort", l1_p1, l2_p1), ("fibreChannelPort", l1_p1, l2_p1), ("adapter", l1_p2, l2_p2) ] :
            for i in xml_root.findall(".//" + t1 + l1_p) :
                for j in i.findall(l2_p + t1) :
                    for k in j.findall("./next_hop") :
                        a_ident = i.get("deviceID")

                        if a_ident in self._flat_map and j.get("existence") == "physical":
                            a_ident = self._flat_map[a_ident]
                        elif a_ident in self._flat_map:
                            print "WARNING: Drop logical connection at ", i.get("deviceID"), " id=", j.get("id")

                            continue

                        b_ident = k.get("deviceID")

                        if b_ident in self._flat_map and j.get("existence") == "physical":
                            b_ident = self._flat_map[b_ident]
                        elif b_ident in self._flat_map:
                            print "WARNING: Drop logical connection at ", i.get("deviceID"), " id=", j.get("id")

                            continue

                        a = (a_ident, j.get("id"))
                        b = (b_ident, k.get("portID"))

                        a_ident, a_id = a
                        a_node = self.DC_Element(a_ident, "device", "physical")
                        b_ident, b_id = b
                        b_node = self.DC_Element(b_ident, "device", "physical")

                        if self._net_graph.search_node(a_node) is not None :
                            src  = a
                            src_node = self._net_graph.search_node(a_node)
                            dest = b
                            dest_node = b_node

                            src_graph = self._net_graph
                        elif self._net_graph.search_node(b_node) is not None :
                            src  = b
                            src_node = self._net_graph.search_node(b_node)
                            dest = a
                            dest_node = a_node

                            src_graph = self._net_graph
                        elif self._ext_graph.search_node(a_node) is not None :
                            src  = a
                            src_node = self._ext_graph.search_node(a_node)
                            dest = b
                            dest_node = b_node

                            src_graph = self._ext_graph
                        elif self._ext_graph.search_node(b_node) is not None :
                            src  = b
                            src_node = self._ext_graph.search_node(b_node)
                            dest = a
                            dest_node = a_node

                            src_graph = self._ext_graph
                        else :
                            print "ERROR: Network net parse observed a mis-connection"

                            raise ValueError

                        src_ident, src_id   = src
                        dest_ident, dest_id = dest

                        if self._net_graph.search_node(dest_node) is not None :
                            dest_graph = self._net_graph
                        elif self._dev_net_graph.search_node(dest_node) is not None :
                            dest_graph = self._dev_net_graph
                        elif self._ext_graph.search_node(dest_node) is not None :
                            dest_graph = self._ext_graph
                        else :
                            print "ERROR: Network net parse observed a mis-connection"

                            raise ValueError

                        dest_node = dest_graph.search_node(dest_node)

                        src_node.add_edge(src_id, dest_node, dest_id)

    def list_layer2(self, subnet="0.0.0.0", netmask="0.0.0.0", layer3=False) :
        retStr = ""

        for i in self._ndev.keys() :
            i_node, i_index = self._ndev[i]

            line_valid = False

            bufStr = "<_key: " + i + " " + str(i_node._tag._ndev[i_index]) + ">\n"

            if layer3 is True :
                for j in self._ninf.keys() :
                    if j.split(":")[0] == i.split(":")[0] :
                        if type(self._ninf[j]) is list :
                            ninf = self._ninf[j]
                        else :
                            ninf = [self._ninf[j]]

                        for j_node, j_index in ninf :
                            if j_node._tag._ninf[j_index]._id == i_node._tag._ndev[i_index]._id :
                                if j_node._tag._ninf[j_index]._ver == "ip_v4" \
                                    and j_node._tag._ninf[j_index]._ip_v4_is_subnet(subnet, netmask) is True :
                                    line_valid = True

                                    bufStr += " |- <_key: " + j + " " + str(j_node._tag._ninf[j_index]) + ">\n"
            else :
                line_valid = True

            if line_valid is True :
                retStr += bufStr

        return retStr

    def list_layer3(self, subnet="0.0.0.0", netmask="0.0.0.0", layer2=True) :
        retStr = ""

        for i in self._ninf.keys() :
            if type(self._ninf[i]) is list :
                ninf = self._ninf[i]
            else :
                ninf = [self._ninf[i]]

            if layer2 is True :
                for i_node, i_index in ninf :
                    if i_node._tag._ninf[i_index]._ver == "ip_v4" \
                        and i_node._tag._ninf[i_index]._ip_v4_is_subnet(subnet, netmask) is True :
                        for j in self._ndev.keys() :
                            if j.split(":")[0] == i.split(":")[0] :
                                j_node, j_index = self._ndev[j]

                                if j_node._tag._ndev[j_index]._id == i_node._tag._ninf[i_index]._id :
                                    retStr += "<_key: " + i + " " + str(j_node._tag._ndev[j_index]) + " <_key: " + j + " " + str(i_node._tag._ninf[i_index]) + "> >\n"
            else :
                for i_node, i_index in ninf :
                    retStr += "<_key: " + i + " " + str(i_node._tag._ninf[i_index]) + ">\n"

        return retStr

    _acct_t = type(Account("abcdefgh", "ijklmnop", "admin"))
    _srvc_t = type(Service("abcdefgh"))

    def __init__(self, xml_file, admin_auth=False) :
        self._tree = None
        self._acct = None
        self._srvc = None
        self._pwr_graph = None
        self._net_graph = None
        self._ndev = None
        self._ninf = None
        self._dev_net_graph = None
        self._dev_pwr_graph = None
        self._ext_graph = None
        self._flat_map = None
        self._flat_lst = None
        self._admin = admin_auth

        tree=ET.parse(xml_file)
        root=tree.getroot()

        self._initialise_flatten_map(root)
        self._initialise_tree(root)
        self._initialise_graph(root)

    def __str__(self) :
        return str(self._tree)

